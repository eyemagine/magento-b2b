<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * B2B Customer Restrictions
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_B2b
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class Eyemagine_B2b_Model_Observer
{
    /**
     * @var Eyemagine_B2b_Helper_Data $helper
     */
    public $helper;
    /**
     * @var Mage_Checkout_Model_Session $checkout
     */
    public $checkout;
    /**
     * @var Mage_Customer_Model_Session $customer
     */
    public $customer;
    /**
     * @var int|null $_subtotalMin
     */
    protected $_subtotalMin = null;
    /**
     * Initialize helper and save checkout session
     *
     * @return void
     */
    public function __construct()
    {
        $this->helper   = Mage::helper('b2b');
        $this->checkout = Mage::getSingleton("checkout/session");
        $this->customer = Mage::getSingleton("customer/session");
    }
    /**
     * Verifies subtotal meets requirements on cart page
     *
     * @return void
     */
    public function checkTotalsCart()
    {
        if ($this->_hasCartError()) {
            $this->_setErrorMessage();
        }
    }
    /**
     * Verifies subtotal meets requirements on checkout
     * 
     * @return void
     */
    public function checkTotalsCheckout()
    {
        if ($this->_hasCartError()) {
            Mage::app()->getFrontController()->getResponse()->setRedirect('/checkout/cart');
        }
    }
    public function unsetErrorMessage()
    {
        Mage::getSingleton("b2b/session")->unsetAll();
    }
    /**
     * Verifies subtotal meets requirements
     *
     * @return bool
     */
    protected function _hasCartError()
    {
        if (!$this->helper->isEnabled() || !$this->_isCustomerB2b()) {
            return false;
        }
        return Mage::getSingleton('checkout/cart')->getQuote()->getSubtotal() < $this->getSubtotalMin();
    }
    /**
     * Check if current customer session is a B2B customer
     * 
     * @return bool
     */
    protected function _isCustomerB2b()
    {
        if (!$this->customer->isLoggedIn()) {
            return false;
        }
        return in_array(Mage::getSingleton('customer/session')->getCustomerGroupId(), $this->helper->getB2bCustomerGroupIds());
    }
    /**
     * Fetches the number of orders this customer has made
     * 
     * @return int
     */
    public function getSubtotalMin()
    {
        if (is_null($this->_subtotalMin)) {
            if ($this->_getCustomerOrderCount() > 0) {
                $this->_subtotalMin = $this->helper->getSubtotalMinSubsequent();
            }
            else {
                $this->_subtotalMin = $this->helper->getSubtotalMin();
            }
        }
        return $this->_subtotalMin;
    }
    /**
     * Returns the number of a times the customer has placed an order
     * 
     * @return int
     */
    protected function _getCustomerOrderCount()
    {
        return Mage::getModel('sales/order')->getCollection()
            ->addFieldToFilter("customer_id", $this->customer->getId())
            ->getSize();
    }
    /**
     * Sets the error message when the minimum subtotal is not met
     * 
     * @return void
     */
    protected function _setErrorMessage()
    {
        $session = Mage::getSingleton("b2b/session");
        if ($session->getIsErrorMessageAdded()) {
            return;
        }
        $session->addError(str_replace("%min%",
                Mage::helper('core')->currency($this->getSubtotalMin(), true, false),
                $this->helper->getErrorMessage()));
        $session->setIsErrorMessageAdded(true);
    }
}
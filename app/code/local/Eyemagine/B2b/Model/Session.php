<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * B2B Customer Restrictions
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_B2b
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class Eyemagine_B2b_Model_Session extends Mage_Core_Model_Session
{
    /**
     * Unset all data associated with object
     */
    public function unsetAll()
    {
        parent::unsetAll();
        $this->_isErrorMessageAdded = null;
    }
}
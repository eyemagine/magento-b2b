<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * B2B Customer Restrictions
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_B2b
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

class Eyemagine_B2b_Helper_Data extends Mage_Core_Helper_Abstract
{
    const MODULE_CONFIG_SECTION = 'sales';
    /**
     * Checks if the module is enabled
     * 
     * @return int
     */
    public function isEnabled()
    {
        return $this->_getConfig("enabled");
    }
    /**
     * Retrieves the configured B2B customer groups
     * 
     * @return array
     */
    public function getB2bCustomerGroupIds()
    {
        return explode(",", $this->_getConfig("b2b_customer_groups"));
    }
    /**
     * Fetches the minimum total required for first order
     * 
     * @return int
     */
    public function getSubtotalMin()
    {
        return $this->_getConfig("subtotal_min");
    }
    /**
     * Fetches the minimum total required for subsequent orders
     * 
     * @return int
     */
    public function getSubtotalMinSubsequent()
    {
        return $this->_getConfig("subtotal_min_subsequent");
    }
    /**
     * Fetches the error message
     * 
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->_getConfig("error_message");
    }
    /**
     * Config fetcher
     * 
     * @param string $field
     * @param string $group
     * 
     * @return int|string
     */
    protected function _getConfig($field, $group = 'b2b')
    {
        return Mage::getStoreConfig(self::MODULE_CONFIG_SECTION . "/$group/$field");
    }
}
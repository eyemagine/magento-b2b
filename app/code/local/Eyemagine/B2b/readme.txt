/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * B2B Customer Restrictions
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_B2b
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

-------------------------------------------------------------------------------
DESCRIPTION:
-------------------------------------------------------------------------------

B2B Customer Restrictions

Module Files:

  - app/etc/modules/B2b.xml
  - app/code/local/Eyemagine/B2b/*


-------------------------------------------------------------------------------
COMPATIBILITY:
-------------------------------------------------------------------------------

  - Magento Enterprise Edition 1.10.0 to 1.12.0
  

-------------------------------------------------------------------------------
RELEASE NOTES:
-------------------------------------------------------------------------------

v1.0.0: August 6, 2013
  - Initial release